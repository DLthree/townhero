//
//  Loading.m
//  TownHero
//
//  Created by Danny Loffredo on 2/8/12.
//  Copyright 2012 Private. All rights reserved.
//

#import "Loading.h"
#import "Battle.h"
#import "GameData.h"

@implementation LoadingLayer


+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	LoadingLayer *layer = [LoadingLayer node];
    
	[scene addChild: layer];

	// return the scene
	return scene;
}


// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
    
	// don't forget to call "super dealloc"
	[super dealloc];
}

////////////////////////////////////////////////////////////////////////////////


// on "init" you need to initialize your instance
-(id) init
{
    
    if((self = [super init])) {
        CCDirector *director = [CCDirector sharedDirector];
        CGSize size = [director winSize];
        CCSprite *sprite = [CCSprite spriteWithFile:@"loading.png"];
        sprite.position = ccp(size.width/2, size.height/2);
        [self addChild:sprite];        
        [self scheduleOnce:@selector(loadResources) delay:0.0f];
    }
    return self;
    
}

-(void) loadResources
{
    
    NSArray* entities = [GameData objectsFromQuery:@"select * from entities" class:[EntityInfo class]];
    for(EntityInfo* info in entities) {
        [Entity loadEntityResources:info];
    }
    
    [self loadNextScene];
    
}

-(void) loadNextScene
{
    CCDirector *director = [CCDirector sharedDirector];
    CCScene* next = [BattleLayer scene];
    [director replaceScene:[CCTransitionFade transitionWithDuration:2.0f scene:next]];
}




@end
