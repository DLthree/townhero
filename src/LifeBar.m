//
//  LifeBar.m
//  TownHero
//
//  Created by Danny Loffredo on 1/10/12.
//  Copyright 2012 Private. All rights reserved.
//

#import "LifeBar.h"


@implementation LifeBar

@synthesize background, progressTimer;


-(id)initWithFile:(NSString*)filename andBackgroundFile:(NSString*)bgFilename
{
    if((self = [super init])) {
        progressTimer = [CCProgressTimer progressWithFile:filename];
        progressTimer.type = kCCProgressTimerTypeHorizontalBarLR;

        progressTimer.percentage = 100;
        _max = 100;
        _value = 100;
        
        if(bgFilename) {
            background = [CCSprite spriteWithFile:bgFilename];
            [self addChild:background];
        }

        [self addChild:progressTimer];
        
        int width = MAX(background.contentSize.width, progressTimer.contentSize.width);
        int height = MAX(background.contentSize.height, progressTimer.contentSize.height);
        contentSize_ = CGSizeMake(width, height);        
        
    }
    return self;
}

-(void)setOpacity:(GLubyte)opacity
{
    _opacity = opacity;
    [progressTimer.sprite setOpacity:opacity];
    [background setOpacity:opacity];
    [progressTimer updateColor];
}

-(GLubyte)opacity
{
    return _opacity;
}

-(id)initWithFile:(NSString*)filename 
{
    return [self initWithFile:filename andBackgroundFile:nil];
}

-(void)setMax:(float)max
{
    _max = max;
    progressTimer.percentage = 100.f * _value / _max;
    
}

-(void)setValue:(float)value
{
    _value = value;
    progressTimer.percentage = 100.f * _value / _max;
}

-(float)max
{
    return _max;
}

-(float)value
{
    return _value;
}

@end
