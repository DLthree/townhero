//
//  Loading.h
//  TownHero
//
//  Created by Danny Loffredo on 2/8/12.
//  Copyright 2012 Private. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface LoadingLayer : CCLayer {
    
}

-(void) loadResources;
-(void) loadNextScene;

+(CCScene *) scene;

@end
