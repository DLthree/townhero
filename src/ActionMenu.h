//
//  ActionMenu.h
//  TownHero
//
//  Created by Danny Loffredo on 1/28/12.
//  Copyright 2012 Private. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCSpriteScale9.h"

@class CCMenuAdvanced;
@class Entity;

@interface ActionMenu : CCNode {
    CCMenuAdvanced* menu;
    CCSpriteScale9* bg;
    
    Entity* entity;
}

-(id) initWithDelegate:(id)delegate entity:(Entity*)e;


@property(nonatomic, retain) CCMenuAdvanced* menu;

@end
