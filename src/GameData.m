//
//  GameData.m
//  TownHero
//
//  Created by Danny Loffredo on 3/18/12.
//  Copyright (c) 2012 Private. All rights reserved.
//

#import "GameData.h"
#import "SynthesizeSingleton.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"

@implementation GameData

SYNTHESIZE_SINGLETON_FOR_CLASS(GameData);

@synthesize db;

- (void) dealloc
{
    [db release];
    
	// don't forget to call "super dealloc"
	[super dealloc];
}

-(id)init 
{
    if((self = [super init])) {
        db = [GameData open:@"db.sqlite"];      
    }
    return self;    
}

+ (FMDatabase*)getDatabase 
{
    GameData* data = [GameData sharedGameData];
    return data.db;
}


+(FMDatabase*) open:(NSString*)filename
{
	NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filename];
	FMDatabase* db = [[FMDatabase databaseWithPath:defaultDBPath] retain];
    
	if (![db open]) {
        [db release];
        return nil;
	}
    return db;    
}

+(NSArray*)objectsFromQuery:(NSString*)query class:(Class)class
{
    GameData* data = [GameData sharedGameData];
    return [GameData objectsFromQuery:query db:data.db class:class];    
}

+(NSArray*)objectsFromQuery:(NSString*)query db:(FMDatabase*)db class:(Class)class
{
    NSMutableArray* objects = [NSMutableArray new];
    FMResultSet *rs = [db executeQuery:query];
    while ([rs next]) {
        id object = [[class alloc] init];
        [rs kvcMagic:object];
        [objects addObject:object];
    }        
    return objects;
}

@end
