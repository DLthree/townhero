//
//  AttachableNode.h
//  TownHero
//
//  Created by Danny Loffredo on 1/10/12.
//  Copyright 2012 Private. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"


@interface AttachableNode : CCNode {
    CCNode* attachee;
    CGPoint offset;
    
}

-(void)attachTo:(CCNode*)node atOffset:(CGPoint)offs;
-(void)tick:(ccTime)dt;

@property(nonatomic, assign, readwrite) CGPoint offset;


@end
