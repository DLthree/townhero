//
//  GameData.h
//  TownHero
//
//  Created by Danny Loffredo on 3/18/12.
//  Copyright (c) 2012 Private. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FMDatabase;

@interface GameData : NSObject {
    FMDatabase* db;
}

+ (GameData *)sharedGameData;
+ (FMDatabase*)getDatabase;

+(FMDatabase*) open:(NSString*)filename;

+(NSArray*)objectsFromQuery:(NSString*)query class:(Class)class;
+(NSArray*)objectsFromQuery:(NSString*)query db:(FMDatabase*)db class:(Class)class;

@property (nonatomic, retain) FMDatabase* db;

@end
