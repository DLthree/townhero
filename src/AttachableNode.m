//
//  AttachableNode.m
//  TownHero
//
//  Created by Danny Loffredo on 1/10/12.
//  Copyright 2012 Private. All rights reserved.
//

#import "AttachableNode.h"

@implementation AttachableNode

@synthesize offset;

-(void)attachTo:(CCNode*)node atOffset:(CGPoint)offs
{
    attachee = node;
    offset = offs;
}

-(void)onEnter
{
    [super onEnter];
    [self schedule:@selector(tick:)];
}

-(void)tick:(ccTime)dt
{
    if(attachee) {
        self.position = ccpAdd([attachee position], offset); 
    }
}


@end
