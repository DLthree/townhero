//
//  ScrollingInfo.h
//  TownHero
//
//  Created by Danny Loffredo on 1/13/12.
//  Copyright 2012 Private. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "AttachableNode.h"

@class Entity;

@interface ScrollingInfo : AttachableNode {
    CCSprite* info;
    
}

-(id)initWithImage:(NSString*)filename onEntity:(Entity*)node;
-(void)animationEnded;

@end
