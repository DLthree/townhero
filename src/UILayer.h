//
//  UILayer.h
//  TownHero
//
//  Created by Danny Loffredo on 11/29/11.
//  Copyright 2011 Private. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class ActionMenu;
@class BattleLayer;
@class Entity;

enum {
    kUIStateIdle,
    kUIStateSelectingMovement,
    kUIStateSelectingAttack,
    kUIStateMenu,
    
};

@interface UILayer : CCLayer {
    ActionMenu *menu;
    BattleLayer *battle;
    CCLabelTTF *messageLabel;
    
    CGPoint touchCoord;
    CGPoint prevCoord;
    Entity* touchEntity;
    BOOL _tap;
    BOOL _touching;
    
    NSMutableDictionary *paths;
    
}

-(void)showMenu:(Entity*)entity;
-(void)hideMenu;
-(void)showMessage:(NSString*)msg;
-(void)tapAt:(CGPoint)coord;
-(void)powerSelected:(id)sender;


@property (nonatomic, retain) BattleLayer * battle;
@property (nonatomic, readonly) ActionMenu* menu;

@end
