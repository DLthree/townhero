//
//  NavigateTo.h
//  TownHero
//
//  Created by Danny Loffredo on 1/16/12.
//  Copyright 2012 Private. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class Entity;

@interface NavigateTo : CCAction {
    
    NSMutableArray* _path;
    Entity* _entity;
    CCActionInterval* move;
    int _index;
    BOOL _done;
    SEL _s;
    
}

+(id) actionWithPath:(NSMutableArray*)path andNotification:(SEL) s;

-(id) initWithPathArray:(NSMutableArray*)path andNotification:(SEL) s;

-(CGPoint) getDestinationForIndex:(int)index;
-(void) moveTo:(CGPoint)dst withCount:(int)count;
-(void) setupNextAnimation;

@end
