//
//  ActionMenu.m
//  TownHero
//
//  Created by Danny Loffredo on 1/28/12.
//  Copyright 2012 Private. All rights reserved.
//

#import "ActionMenu.h"
#import "CCMenuAdvanced.h"
#import "CCSpriteScale9.h"
#import "Entity.h"
#import "UILayer.h"
#import "Power.h"

@implementation ActionMenu

@synthesize menu;

#define MENU_PADDING 5.f
#define MENU_BORDER 5.f

#define BG_OPACITY 150

-(CCMenuItem*) createMenuItem:(NSString*)filename target:(id)target selector:(SEL)s
{
    CCMenuItemImage* item = [CCMenuItemImage itemFromNormalImage:filename selectedImage:filename target:target selector:s];
    return item;
}

// on "init" you need to initialize your instance
-(id) initWithDelegate:(id)delegate entity:(Entity*)e
{
    
    if((self = [super init])) {
        
        entity = e;
        
        menu = [CCMenuAdvanced menuWithItems:nil];
        for(Power* power in [e powers]) {
            CCMenuItem* item = [self createMenuItem:power.icon target:delegate selector:@selector(powerSelected:)];
            [item setUserData:power];
            [menu addChild:item];            
        }
        
        menu.itemAlignment = kItemsAlignedLeft;
        [menu alignItemsHorizontallyWithPadding:MENU_PADDING leftToRight:YES];
        menu.positionInPixels = ccp(MENU_BORDER,MENU_BORDER);
        menu.anchorPoint = ccp(0, 0);
        
        bg = [CCSpriteScale9 spriteWithFile:@"lifebar_bg.png" andLeftCapWidth:3.f andTopCapHeight:3.f];
        bg.anchorPoint = ccp(0, 0);
        bg.opacity = BG_OPACITY;

        CGSize size = CGSizeMake(menu.contentSize.width + MENU_BORDER*2, menu.contentSize.height + MENU_BORDER*2);
        [bg adaptiveScale9:size];
        
        [self addChild:bg];
        [self addChild:menu];
        
        [self setContentSize:size];        
        
        self.anchorPoint = ccp(0.5, 1);
        
    }
    return self;
}

-(void)setOpacity:(GLubyte)opacity
{
    bg.opacity = BG_OPACITY / 255. * opacity;
    menu.opacity = opacity;
}


@end
