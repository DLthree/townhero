//
//  Battle.h
//  TownHero
//
//  Created by Danny Loffredo on 10/22/11.
//  Copyright Private 2011. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "Entity.h"
#import "UILayer.h"
#import "Player.h"
#import "Board.h"

#define HUD_Z 1000

// BattleLayer
@interface BattleLayer : CCLayer
{

    Board *_board;
    
    UILayer* _uiLayer;
    BOOL _tap;
    BOOL _touching;
    
    CGPoint _viewpointCenter;
    
    NSMutableSet *_enemies;
    NSMutableSet *_players;
    
    BOOL playerTurn;
    
}

// returns a CCScene that contains the BattleLayer as the only child
+(CCScene *) scene;

@property (nonatomic, retain) UILayer *uiLayer;
@property (nonatomic, retain) Board *board;

-(void) start;

-(void) setViewpointCenter:(CGPoint) position;
-(void) tick:(ccTime)dt;
-(Entity*) initEntity:(Entity*)e at:(CGPoint)position;
-(void) startPlayerTurn;
-(void) startEnemyTurn;
-(void) setDynamicZ:(id)obj;
-(void) entityTurnCompleteNotification:(Entity*)e;


@property (nonatomic, retain) NSMutableSet *enemies;
@property (nonatomic, retain) NSMutableSet *players;

@end

