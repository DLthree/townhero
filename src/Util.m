//
//  Util.m
//  TownHero
//
//  Created by Danny Loffredo on 12/5/11.
//  Copyright (c) 2011 Private. All rights reserved.
//

#import "Util.h"

void drawRectangle(CGRect rect) 
{
    CGPoint vertices[4];
    vertices[0] = rect.origin;
    vertices[1] = ccpAdd(rect.origin, ccp(0, rect.size.height));
    vertices[2] = ccpAdd(rect.origin, ccp(rect.size.width, rect.size.height));
    vertices[3] = ccpAdd(rect.origin, ccp(rect.size.width, 0));
    
    ccDrawPoly(vertices, 4, YES);
    
}


NSValue* wrap(CGPoint p)
{
    return [NSValue valueWithCGPoint:p];   
}

CGPoint unwrap(NSValue* value) 
{
    return [value CGPointValue];
}

CGPoint winPoint(float x, float y)
{
    CGSize s = [CCDirector sharedDirector].winSize;
    return ccp(s.width * x, s.height * y);    
}

CGPoint clampToScreen(CGRect bb)
{
    CGSize s = [CCDirector sharedDirector].winSize;

    CGPoint fixup = ccp(0,0);
    
    float max_x = CGRectGetMaxX(bb);
    float max_y = CGRectGetMaxY(bb);
    float min_x = CGRectGetMinX(bb);
    float min_y = CGRectGetMinY(bb);
    
    
    if(max_x > s.width) {
        fixup.x = s.width - max_x;
    } else if(min_x < 0) {
        fixup.x = -min_x;
    }

    if(max_y > s.height) {
        fixup.y = s.height - max_y;
    } else if(min_y < 0) {
        fixup.y = -min_y;
    }
    
    return fixup;
}
