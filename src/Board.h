//
//  Board.h
//  TownHero
//
//  Created by Danny Loffredo on 12/30/11.
//  Copyright 2011 Private. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

#define MAX_ROWS 100
#define MAX_COLS 100

#define BASE_IMAGE_NUM 0

#define NO_COLOR 0
#define BLUE_COLOR    (BASE_IMAGE_NUM + 1)
#define CYAN_COLOR    (BASE_IMAGE_NUM + 2)
#define GREEN_COLOR   (BASE_IMAGE_NUM + 3)
#define MAGENTA_COLOR (BASE_IMAGE_NUM + 5)
#define RED_COLOR     (BASE_IMAGE_NUM + 6)
#define YELLOW_COLOR  (BASE_IMAGE_NUM + 7)

#define NO_HIGHLIGHT 0
#define DARK_HIGHLIGHT    (BASE_IMAGE_NUM + 4)
#define LIGHT_HIGHLIGHT   (BASE_IMAGE_NUM + 8)

@class Entity;


@interface TileInfo : NSObject {
    BOOL collision;
    Entity* entity;
}

@property (nonatomic, readwrite, assign) BOOL collision;
@property (nonatomic, retain) Entity* entity;

@end

@interface Board : CCTMXTiledMap {

    CCTMXLayer *_collision;
    CCTMXLayer *_background;
    CCTMXLayer *_foreground;
    CCTMXLayer *_highlights;
    CCTMXObjectGroup *_objects;
    
    TileInfo* _tiles[MAX_ROWS][MAX_COLS];
    
}

-(void)setEntity:(Entity*)e at:(CGPoint)coord;
-(void)moveEntity:(Entity*)e from:(CGPoint)fromCoord to:(CGPoint)toCoord;
-(void)entityChangedAt:(CGPoint)coord;

-(CGPoint) positionFromTileCoord:(CGPoint)coord;
-(CGPoint) tileCoordFromPosition:(CGPoint)position;
-(CGPoint) fromObjectLayerToTileCoord:(NSMutableDictionary*) object;
-(CGPoint) fromObjectLayerToPosition:(NSMutableDictionary*) object;
-(BOOL) checkForCollision:(CGPoint)tileCoord;
-(float) getDynamicZ:(id)obj;
-(NSMutableDictionary*) objectNamed:(NSString*)name;

-(TileInfo*) tileInfoAt:(CGPoint)coord;
-(CGPoint) getCenter;

-(NSMutableArray*) neighbors:(CGPoint)coord;
-(BOOL) walkable:(CGPoint)coord byEntity:(Entity*)e;
-(void)setHighlight:(int)highlight at:(CGPoint)coord;

-(void) colorAllTiles:(int)color;
-(void) colorTiles:(NSArray*)tiles color:(int)color;

@property (nonatomic, retain) CCTMXObjectGroup *objects;

@end
