//
//  EntityHud.m
//  TownHero
//
//  Created by Danny Loffredo on 1/11/12.
//  Copyright 2012 Private. All rights reserved.
//

#import "EntityHud.h"
#import "Entity.h"


@implementation EntityHud

// on "init" you need to initialize your instance
-(id) initWithEntity:(Entity*)node
{
    
    if((self = [super init])) {
        
        [super attachTo:node atOffset:ccp(0,0)];
        entity = (Entity*)node;
        
        if(entity.entityType == kEntityPlayer) {
            healthBar = [[LifeBar alloc] initWithFile:@"lifebar_green.png" andBackgroundFile:@"lifebar_bg.png"];
        } else if(entity.entityType == kEntityEnemy) {
            healthBar = [[LifeBar alloc] initWithFile:@"lifebar_red.png" andBackgroundFile:@"lifebar_bg.png"];
        }
        healthBar.opacity = 100;
        [self addChild:healthBar];
        
        healthBar.max = entity.healthMax;
        healthBar.value = entity.healthCurrent;
        
        
        entity.hud = self;
//        self.visible = NO;
        
    }
    return self;
}

-(void)tick:(ccTime)dt
{
    [super tick:dt];
    healthBar.value = entity.healthCurrent;
    
    
}

-(void)attachTo:(CCNode*)node atOffset:(CGPoint)offs
{
}

        

@end
