//
//  NavigateTo.m
//  TownHero
//
//  Created by Danny Loffredo on 1/16/12.
//  Copyright 2012 Private. All rights reserved.
//

#import "NavigateTo.h"
#import "Entity.h"
#import "Board.h"
#import "Animation.h"

@implementation NavigateTo

+(id) actionWithPath:(NSMutableArray*)path andNotification:(SEL) s
{
	return [[[self alloc] initWithPathArray:path andNotification:s] autorelease];
}

-(id) initWithPathArray:(NSMutableArray*)path andNotification:(SEL) s
{
	if( (self=[super init]) ) {
        _path = path;
        _s = s;
        
	}
	
	return self;
}

-(void) startWithTarget:(id)aTarget
{
    [super startWithTarget:aTarget];
    _entity = aTarget;
    
    _index = 0;
    _done = NO;
    [self setupNextAnimation];

}

-(void) setupNextAnimation
{
    int numMoves = 1;
    CGPoint dst = [self getDestinationForIndex:_index];
    int facing = getFacing(ccpSub(dst, [_entity position]));
    while(_index + numMoves < [_path count]) {
        CGPoint nextDst = [self getDestinationForIndex:_index + numMoves];
        int nextFacing = getFacing(ccpSub(nextDst, dst));
        if (nextFacing != facing) break;
        dst = nextDst;
        numMoves++;        
    }

    [self moveTo:dst withCount:numMoves];
    _index += numMoves;    
}

-(CGPoint) getDestinationForIndex:(int)index
{
    NSValue* value = [_path objectAtIndex:index];
    CGPoint tileCoord = unwrap(value);
    return [[_entity board] positionFromTileCoord:tileCoord];
}

-(void) moveTo:(CGPoint)dst withCount:(int)count
{
    move = [[CCMoveTo actionWithDuration:count*[_entity moveSpeed] position:dst] retain];
    CGPoint direction = ccpSub(dst, [_entity position]);
    int facing = getFacing(direction);
    [_entity runAnimation:kAnimationRun withFacing:facing repeat:YES];
    [_entity runAction:move];
    

}

-(void) stop
{
    [super stop];
    [move stop];
    [move release];
    move = nil;
    _entity = nil;
}

-(BOOL) isDone
{
	return _done;
}

-(void)step:(ccTime)time 
{
    if([move isDone]) {
        [move release];
        move = nil;
        if(_index >= [_path count]) {
            _done = YES;
            if(_s) {
                [_entity performSelector:_s];
            }
        } else {
            [self setupNextAnimation];            
        }
    }
}

@end
