//
//  DirectionalSprite.m
//  TownHero
//
//  Created by Danny Loffredo on 10/28/11.
//  Copyright 2011 Private. All rights reserved.
//

#import "Animation.h"

// TODO: possibly simplify this with KVC (setValuesForKeysWithDictionary)?
//       see http://theocacao.com/document.page/161

int getFacing(CGPoint p) {
    if(p.x >= 0 && p.y >= 0) return kFacingNorthEast;
    if(p.x >= 0 && p.y <= 0) return kFacingSouthEast;
    if(p.x <= 0 && p.y <= 0) return kFacingSouthWest;
    return kFacingNorthWest;
}

int repeatNumber(NSString* name)
{
    if([name isEqualToString:@"back_forth"]) return kRepeatBounce;
    if([name isEqualToString:@"looped"]) return kRepeatLoop;
    if([name isEqualToString:@"play_once"]) return kRepeatOnce;
    return -1;
}

int animationNumber(NSString* name)
{
    
    if([name isEqualToString:@"critdie"]) return kAnimationCritDie;
    if([name isEqualToString:@"die"]) return kAnimationDie;
    if([name isEqualToString:@"hit"]) return kAnimationHit;
    if([name isEqualToString:@"melee"]) return kAnimationMelee;
    if([name isEqualToString:@"ment"]) return kAnimationMent;
    if([name isEqualToString:@"ranged"]) return kAnimationRanged;
    if([name isEqualToString:@"block"]) return kAnimationBlock;
    if([name isEqualToString:@"run"]) return kAnimationRun;
    if([name isEqualToString:@"stance"]) return kAnimationStance;
    if([name isEqualToString:@"spawn"]) return kAnimationSpawn;
    
    return -1;
    
}

CCAnimation* actionFromDictionary(NSDictionary* dict, int facing, NSString* name, int frames_x, int frames_y)
{
    int type = repeatNumber([dict valueForKey:@"type"]);
    int position = [[dict valueForKey:@"position"] intValue];
    int num_frames = [[dict valueForKey:@"frames"] intValue];
    
    int first_frame = (facing * frames_x) + position;
    int last_frame = first_frame + num_frames - 1;
    
    NSMutableArray* frames = [NSMutableArray array];
    for(int i = first_frame; i <= last_frame; i++) {
        id obj = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                  [NSString stringWithFormat:@"%@%d.png", name, i]];
        [frames addObject:obj];
    }
    
    if(type == kRepeatBounce) {
        for(int i = last_frame - 1; i >= first_frame; i--) {
            id obj = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"%@%d.png", name, i]];
            [frames addObject:obj];
        }
    }

    // TODO figure out animation duration. this doesn't give good results...
    //  at the very least, include "delay" as a parameter in the plist file
//    int duration = [[dict valueForKey:@"duration"] intValue];
//    float delay = (duration / 60.) / [frames count];
    float delay = 0.1f;
    
    return [CCAnimation animationWithFrames:frames delay:delay];
}

void animationsFromPlist(NSString* name, NSString* filename)
{
   
    NSString *path = [CCFileUtils fullPathFromRelativePath:[NSString stringWithFormat:filename, name]];
    NSDictionary *d = [NSDictionary dictionaryWithContentsOfFile:path];
    NSArray* keys = [d allKeys];

    NSDictionary *render = [d objectForKey:@"render"];
    int frames_x = [[render objectForKey:@"frames_x"] integerValue];
    int frames_y = [[render objectForKey:@"frames_y"] integerValue];
    int count = [keys count];
    
    for(int i = 0; i < count; i++) {
        NSString* key = [keys objectAtIndex:i];
        NSDictionary* value = [d objectForKey: key];
        
        int animation = animationNumber(key);
        if(animation < 0) continue;
        
        for(int facing = 0; facing < NUM_FACINGS; facing++) {
            
            CCAnimation *anim = actionFromDictionary(value, facing, name, frames_x, frames_y);
            [[CCAnimationCache sharedAnimationCache] addAnimation:anim name:animationName(name, facing, animation)];
        }
        
    }
}

NSString* animationName(NSString* name, int facing, int animation)
{
    NSString* animName = [NSString stringWithFormat:@"%@ %d.%d", name, animation, facing];
    return animName;
}

