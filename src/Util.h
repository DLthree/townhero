//
//  Util.h
//  TownHero
//
//  Created by Danny Loffredo on 12/5/11.
//  Copyright (c) 2011 Private. All rights reserved.
//

#import "cocos2d.h"

void drawRectangle(CGRect rect);
NSValue* wrap(CGPoint p);
CGPoint unwrap(NSValue* value);

CGPoint winPoint(float x, float y);

CGPoint clampToScreen(CGRect bb);
