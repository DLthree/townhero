//
//  Board.m
//  TownHero
//
//  Created by Danny Loffredo on 12/30/11.
//  Copyright 2011 Private. All rights reserved.
//

#import "Board.h"
#import "Entity.h"
#import "Player.h"
#import "Enemy.h"



@implementation TileInfo
@synthesize entity, collision;
@end

@implementation Board

@synthesize objects = _objects;

// on "init" you need to initialize your instance
-(id) initWithTMXFile:(NSString*)tmxFile
{
    
    if((self = [super initWithTMXFile:tmxFile])) {
        _background = [self layerNamed:@"background"];
        _collision = [self layerNamed:@"collision"];
        _foreground = [self layerNamed:@"foreground"];
        _objects = [self objectGroupNamed:@"objects"];
        _highlights = [self layerNamed:@"highlights"];
        
        [self reorderChild:_background z:1];
        [self reorderChild:_highlights z:3];
        [self reorderChild:_foreground z:4];
       
        _collision.visible = NO;
        // TODO for static bg
//        _background.visible = NO;
//        _foreground.visible = NO;
        
        for(int i = 0; i < self.mapSize.width; i++) {
            for (int j = 0; j < self.mapSize.height; j++) {
                CGPoint coord = ccp(i,j);
                TileInfo* info = [[[TileInfo alloc] init] retain];
                info.entity = nil;
                info.collision = [self checkForCollision:coord];
                _tiles[i][j] = info;
                
            }
        }
        
    }
    return self;
}

       
-(BOOL) checkForCollision:(CGPoint)tileCoord {
    
    if (tileCoord.x < 0 || tileCoord.y < 0 || tileCoord.x >= self.mapSize.width || tileCoord.y >= self.mapSize.height) {
        return YES;
    }
    
    int tileGid = [_collision tileGIDAt:tileCoord];
    if (tileGid) {
        NSDictionary *properties = [self propertiesForGID:tileGid];
        if (properties) {
            NSString *collide = [properties valueForKey:@"collide"];
            if (collide && [collide compare:@"True"] == NSOrderedSame) {
                return YES;
            }
        }
    }
    
    return NO;
    
}


-(void)setHighlight:(int)highlight at:(CGPoint)coord
{
    [_highlights setTileGID:highlight at:coord];
}

-(void)setEntity:(Entity*)e at:(CGPoint)coord
{
    TileInfo* info = [self tileInfoAt:coord];
    NSAssert(info.entity == nil, @"multiple entities at the same location");
    info.entity = e;
    e.coord = coord;
    
    [self entityChangedAt:coord];

    
}

-(void)entityChangedAt:(CGPoint)coord
{
    
}


-(void)moveEntity:(Entity*)e from:(CGPoint)fromCoord to:(CGPoint)toCoord
{
    TileInfo* fromTile = [self tileInfoAt:fromCoord];
    TileInfo* toTile = [self tileInfoAt:toCoord];
    
    NSAssert(fromTile.entity == e && toTile.entity == nil, @"invalid move");
    fromTile.entity = nil;
    toTile.entity = e;
    e.coord = toCoord;
    
    [self entityChangedAt:toCoord];
    [self entityChangedAt:fromCoord];
    
}


- (CGPoint)positionFromTileCoord:(CGPoint)coord {
    
    CGPoint p = [_background positionAt:coord];
    return ccpAdd(p, ccp(self.tileSize.width/2, self.tileSize.height/2));
    
}


-(CGPoint) tileCoordFromPosition:(CGPoint)position
{
    
    float halfMapWidth = self.mapSize.width * 0.5f;
    float mapHeight = self.mapSize.height;
    float tileWidth = self.tileSize.width;
    float tileHeight = self.tileSize.height;
    
    CGPoint tilePosDiv = CGPointMake(position.x / tileWidth, position.y / tileHeight);
    float mapHeightDiff = mapHeight - tilePosDiv.y;
    
    // Cast to int makes sure that result is in whole numbers, tile coordinates will be used as array indices
    int posX = (mapHeightDiff + tilePosDiv.x - halfMapWidth);
    int posY = (mapHeightDiff - tilePosDiv.x + halfMapWidth);
    
    return CGPointMake(posX, posY);
}

-(CGPoint) fromObjectLayerToTileCoord:(NSMutableDictionary*) object
{
    float mapHeight = self.mapSize.height * self.tileSize.height;
    int objectHeight = [[object valueForKey:@"height"] intValue];
    float x = [[object valueForKey:@"x"] intValue] / (self.tileSize.width / 2);
    float y = ((mapHeight - objectHeight) - [[object valueForKey:@"y"] intValue]) / self.tileSize.height;
    return CGPointMake(x, y);
}

-(CGPoint) fromObjectLayerToPosition:(NSMutableDictionary*) object
{
    CGPoint p = [self fromObjectLayerToTileCoord:object];
	return [self positionFromTileCoord:p];
}


-(float) getDynamicZ:(id)obj {
    CGPoint p = [obj position];
    float z = -( -1 + (p.y + (self.tileSize.width/2)) / (self.tileSize.height/2));
    return z;
}

-(NSMutableDictionary*) objectNamed:(NSString*)name
{
    return [_objects objectNamed:name];    
}

-(void) colorAllTiles:(int)color
{
    for(int i = 0; i < self.mapSize.width; i++) {
        for (int j = 0; j < self.mapSize.height; j++) {
            [self setHighlight:color at:ccp(i,j)];
        }
    }
}

-(void) colorTiles:(NSArray*)tiles color:(int)color
{
    for(NSValue* value in tiles) {
        CGPoint destination = [value CGPointValue];
        [self setHighlight:color at:destination];
        
    }
}

-(BOOL) validCoord:(CGPoint)coord
{
    return (coord.x >= 0) && (coord.y >= 0) && (coord.x < mapSize_.width) && (coord.y < mapSize_.height);
}

-(TileInfo*) tileInfoAt:(CGPoint)coord
{
    if([self validCoord:coord]) {
        return _tiles[(int)coord.x][(int)coord.y];    
    }
    return nil;
}


-(CGPoint) getCenter
{
    NSString *x = [self propertyNamed:@"center_x"];
    NSAssert(x, @"no center_x defined");
    
    NSString *y = [self propertyNamed:@"center_y"];
    NSAssert(y, @"no center_y defined");
    
    CGPoint ret = CGPointMake([x intValue], [y intValue]);
    
    return ret;
}



-(NSMutableArray*) neighbors:(CGPoint)coord
{
    NSMutableArray* neighbors = [[NSMutableArray alloc] init ];
    CGPoint p;

    p = ccp(coord.x - 1, coord.y);
    if([self validCoord:p]) [neighbors addObject:[NSValue valueWithCGPoint:p]];

    p = ccp(coord.x + 1, coord.y);
    if([self validCoord:p]) [neighbors addObject:[NSValue valueWithCGPoint:p]];
    
    p = ccp(coord.x, coord.y - 1);
    if([self validCoord:p]) [neighbors addObject:[NSValue valueWithCGPoint:p]];
    
    p = ccp(coord.x, coord.y + 1);
    if([self validCoord:p]) [neighbors addObject:[NSValue valueWithCGPoint:p]];

    return neighbors;
}

-(BOOL) walkable:(CGPoint)coord byEntity:(Entity*)e;
{
    TileInfo *info = [self tileInfoAt:coord];
    if(info == nil) return NO;
    if(info.collision) return NO;
    
    Entity* dstEntity = info.entity;
    if(dstEntity == nil) return YES;
    
    if([e isKindOfClass:[dstEntity class]]) {
        return YES;
    } else {
        return NO;
    }
}

@end
