//
//  LifeBar.h
//  TownHero
//
//  Created by Danny Loffredo on 1/10/12.
//  Copyright 2012 Private. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface LifeBar : CCNode {
    CCProgressTimer *progressTimer;
    CCSprite* background;
    
    float _max;
    float _value;
    GLubyte _opacity;
        
}

-(id)initWithFile:(NSString*)filename;
-(id)initWithFile:(NSString*)filename andBackgroundFile:(NSString*)bgFilename;
-(void)setOpacity:(GLubyte)opacity;
-(GLubyte)opacity;

@property (nonatomic,retain) CCSprite *background;
@property (nonatomic,retain) CCProgressTimer *progressTimer;
@property (nonatomic,assign,readwrite) float max;
@property (nonatomic,assign,readwrite) float value;
@property (nonatomic,assign,readwrite) GLubyte opacity;

@end