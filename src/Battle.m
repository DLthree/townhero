//
//  Battle.m
//  TownHero
//
//  Created by Danny Loffredo on 10/22/11.
//  Copyright Private 2011. All rights reserved.
//

// Import the interfaces
#import "Battle.h"
#import "Animation.h"
#import "Enemy.h"
#import "EntityHud.h"
#import "ActionMenu.h"

// BattleLayer implementation
@implementation BattleLayer

@synthesize players = _players;
@synthesize board = _board;
@synthesize enemies = _enemies;
@synthesize uiLayer = _uiLayer;

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	BattleLayer *battleLayer = [BattleLayer node];
    UILayer* uiLayer = [UILayer node];
    
    [battleLayer setUiLayer:uiLayer];
    [uiLayer setBattle:battleLayer];
    
	[scene addChild: battleLayer];
    [scene addChild: uiLayer];	
    
    [battleLayer start];
	
	// return the scene
	return scene;
}


// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
    self.board = nil;
    
	// don't forget to call "super dealloc"
	[super dealloc];
}

////////////////////////////////////////////////////////////////////////////////


// on "init" you need to initialize your instance
-(id) init
{
    
    if((self = [super init])) {

        self.board = [[Board alloc] initWithTMXFile:@"arena.tmx"];
        [self addChild:_board z:-1000];
        [self setViewpointCenter:[_board getCenter]];

// TODO: static background image?  this doesn't seem to increase performance much        
//        CCSprite* bg = [CCSprite spriteWithFile:@"arena.png"];
//        bg.anchorPoint = ccp(0,0);
//        [self addChild:bg z:-1000];
        
        _enemies = [[NSMutableSet alloc] init];
        _players = [[NSMutableSet alloc] init];

        for(NSMutableDictionary *object in _board.objects.objects) {
            NSString* name = [object valueForKey:@"name"];
            if( [name isEqual:@"player_spawn"] ) {

                NSString* kind = [object valueForKey:@"entity"];
                CGPoint spawnPoint = [_board fromObjectLayerToPosition:object];       
                int facing = [[object valueForKey:@"facing"] integerValue] % NUM_FACINGS;
                Player *p = [[Player alloc] initWithName:kind];
                [self initEntity:p at:spawnPoint];
                [_players addObject:p];
                [p runAnimation:kAnimationStance withFacing:facing repeat:YES];
               
            } else if([name isEqual:@"enemy_spawn"]) {

                NSString* kind = [object valueForKey:@"entity"];
                CGPoint spawnPoint = [_board fromObjectLayerToPosition:object];        
                int facing = [[object valueForKey:@"facing"] integerValue] % NUM_FACINGS;
                Enemy *e = [[Enemy alloc] initWithName:kind];
                [self initEntity:e at:spawnPoint];
                [_enemies addObject:e];
                [e runAnimation:kAnimationStance withFacing:facing repeat:YES];
                
            } else {
                NSLog(@"skipped object: %@", name);
            }
            
        }
      
    }
    return self;
  
}

-(void) start
{
    [self schedule:@selector(tick:)];
    [self startPlayerTurn];
}

-(void) startPlayerTurn 
{
    playerTurn = YES;
    [_uiLayer showMessage:@"Hero Turn"];
    for(Player* p in _players) {
        [p newTurn];        
    }
}

-(void) endPlayerTurn
{
    [self startEnemyTurn];   
}

-(void) endEnemyTurn
{
    [self startPlayerTurn];   
}

-(void) startEnemyTurn
{
    playerTurn = NO;
    [_uiLayer showMessage:@"Enemy Turn"];
    // TODO: temporary until enemies do things
    [self scheduleOnce:@selector(endEnemyTurn) delay:3.0f];
}

-(void) entityTurnCompleteNotification:(Entity*)e
{
    if(e.entityType != kEntityPlayer) return;
    
    for(Player* p in _players) {
        if(!p.turnComplete) {
            return;
        }
    }
    
    [self startEnemyTurn];
}

-(Entity*) initEntity:(Entity*)e at:(CGPoint)position
{
    e.position = position;
    [self addChild:e];
    [self setDynamicZ:e];
    
    [_board setEntity:e at:[_board tileCoordFromPosition:position]];

    EntityHud* hud = [[EntityHud alloc] initWithEntity:e];
    [self addChild:hud z:HUD_Z];
    
    return e;
}
-(void) setDynamicZ:(id)obj
{
    float z = [_board getDynamicZ:obj];
    [self reorderChild:obj z:z];    
}
////////////////////////////////////////////////////////////////////////////////


-(void)setViewpointCenter:(CGPoint) position
{
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];

    int x = MAX(position.x, winSize.width / 2);
    int y = MAX(position.y, winSize.height / 2);
    x = MIN(x, (_board.mapSize.width * _board.tileSize.width) 
            - winSize.width / 2);
    y = MIN(y, (_board.mapSize.height * _board.tileSize.height) 
            - winSize.height/2);
    CGPoint actualPosition = ccp(x, y);
    _viewpointCenter = actualPosition;
    
    CGPoint centerOfView = ccp(winSize.width/2, winSize.height/2);
    CGPoint viewPoint = ccpSub(centerOfView, actualPosition);

    self.position = viewPoint;
}

-(void) tick:(ccTime)dt
{

}

////////////////////////////////////////////////////////////////////////////////



// TODO any special draw code here
//-(void)draw
//{
//    
//    [super draw];
//    
//}

@end


