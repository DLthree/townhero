//
//  Player.m
//  TownHero
//
//  Created by Danny Loffredo on 10/28/11.
//  Copyright 2011 Private. All rights reserved.
//

#import "Entity.h"
#import "Animation.h"
#import "Battle.h"
#import "Board.h"
#import "AttachableNode.h"
#import "EntityHud.h"
#import "ScrollingInfo.h"
#import "NavigateTo.h"
#import "Power.h"
#import "GameData.h"

@implementation EntityInfo

@synthesize name, animationFile, framesFile, textureFile, health, meleeDamageMin, meleeDamageMax, moveRange;

@end

@implementation Entity

@synthesize name = _name;
@synthesize coord = _coord;
@synthesize board = _board;

@synthesize healthCurrent, healthMax, entityType, hud, meleeDamageMax, meleeDamageMin, missChance, critChance, turnComplete;
@synthesize movementReady, attackReady, alive, currentFacing, currentAnimation, moveSpeed, powers;


+ (void)loadEntityResources:(EntityInfo*)info {
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:info.framesFile textureFile:info.textureFile];
    animationsFromPlist(info.name, info.animationFile);
    
}



// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// don't forget to call "super dealloc"
	[super dealloc];
}

// on "init" you need to initialize your instance
-(id) initWithName:(NSString*)name
{
    if((self = [super initWithSpriteFrameName:[NSString stringWithFormat:@"%@0.png", name]])) {

        NSString* query = [NSString stringWithFormat:@"select * from entities where name = \"%@\"", name];
        NSArray* entities = [GameData objectsFromQuery:query class:[EntityInfo class]];
        NSAssert([entities count] == 1, @"database error retrieving entity");
        EntityInfo* info = [entities objectAtIndex:0];

        _battle = nil;
        _board = nil;
        _name = name;
        moveSpeed = .3f;
        
        alive = YES;
        movementReady = NO;
        attackReady = NO;

        healthMax = [info.health intValue];
        healthCurrent = healthMax;
        meleeDamageMax = [info.meleeDamageMax intValue];
        meleeDamageMin = [info.meleeDamageMin intValue];
        _moveRange = [info.moveRange intValue];

        anchor = ccp(0.5, 0.25);
        [self setAnchorPoint:anchor]; 
        
        
        // TODO: temporary
        powers = [NSMutableArray new];
        Power* power;
        
        power = [Power new];
        power.icon = @"enchant-red-3.png";
        [powers addObject:power];
        
        power = [Power new];
        power.icon = @"protect-orange-2.png";
        [powers addObject:power];
        
        power = [Power new];
        power.icon = @"heal-jade-2.png";
        [powers addObject:power];
        
        power = [Power new];
        power.icon = @"fireball-red-1.png";
        [powers addObject:power];
        
        power = [Power new];
        power.icon = @"wind-grasp-sky-1.png";
        [powers addObject:power];
        
    }
    return self;
}

-(void)onEnter
{
    [super onEnter];
    CCNode* p = [self parent];
    NSAssert([p isKindOfClass:[BattleLayer class]], @"entity expects BattleLayer as a parent");
    _battle = (BattleLayer*)p;
    _board = _battle.board;

    [self schedule:@selector(tick:)];
   
}

- (void)onExit
{
	[super onExit];
}

#define ANIMATION_TAG 0x100

-(void)runAnimation:(int)animation withFacing:(int)facing withNotification:(SEL)s repeat:(BOOL)repeat
{

    CCAnimation* anim = [[CCAnimationCache sharedAnimationCache] animationByName:animationName(self.name, facing, animation)];
    CCAnimate* action = [CCAnimate actionWithAnimation:anim restoreOriginalFrame:NO];

    if(s) {
        CCCallFunc* call = [CCCallFunc actionWithTarget:self selector:s];
        action = [CCSequence actions:action, call, nil];        
    }  
    
    if (repeat) {
        action = [CCRepeatForever actionWithAction:action];
    }

    [self stopActionByTag:ANIMATION_TAG];
    [action setTag:ANIMATION_TAG];
    [self runAction:action];

    currentAnimation = animation;
    currentFacing = facing;
}

-(void)runAnimation:(int)animation withFacing:(int)facing withNotification:(SEL)s
{
    [self runAnimation:animation withFacing:facing withNotification:s repeat:NO];
    
}

-(void)runAnimation:(int)animation withFacing:(int)facing
{
    [self runAnimation:animation withFacing:facing withNotification:nil repeat:NO];
}


-(void)runAnimation:(int)animation withFacing:(int)facing repeat:(BOOL)repeat
{
    [self runAnimation:animation withFacing:facing withNotification:nil repeat:YES];
    
}
////////////////////////////////////////////////////////////////////////////////



-(void)meleeHit:(Entity*)entity
{
    int damage = [self calculateMeleeDamage];
    [entity receiveDamage:damage from:self delay:0.2f];
}

-(int) calculateMeleeDamage
{
    int range = meleeDamageMax - meleeDamageMin + 1;
    return (random() % range) + meleeDamageMin;
}

 
-(void)receiveDamage:(int)damage from:(Entity*)entity delay:(float)delay
{
    if(!alive) return;
    
    healthCurrent = healthCurrent - damage;
    CCCallBlock* call;
    if(healthCurrent < 0) {
        alive = NO;
        call = [CCCallBlock actionWithBlock:^{
            [self runAnimation:kAnimationDie withFacing:currentFacing];
        }];        
    } else {
        CGPoint difference = ccpSub(entity.position, self.position);
        call = [CCCallBlock actionWithBlock:^{
            [self runAnimation:kAnimationHit withFacing:getFacing(difference) withNotification:@selector(hitEndedNotification)];
        }];
    }
    CCDelayTime* wait = [CCDelayTime actionWithDuration:delay];
    [self runAction:[CCSequence actions:wait, call, nil]];
}

-(void)hitEndedNotification
{
    [self runAnimation:kAnimationStance withFacing:currentFacing repeat:YES];
}

-(void)attack:(CGPoint)coord
{
    
    TileInfo* tileInfo = [_board tileInfoAt:coord];
    Entity* target = tileInfo.entity;

    if(target) {
        [self meleeHit:target];
    }
    attackReady = NO;
    
    CGPoint difference = ccpSub([_board positionFromTileCoord:coord], self.position);
    
    [self runAnimation:kAnimationMelee withFacing:getFacing(difference) withNotification:@selector(meleeAttackEndedNotification)];
  
}

-(void)meleeAttackEndedNotification
{
//    [self resetActionTimer];
    [self runAnimation:kAnimationStance withFacing:currentFacing repeat:YES];    
    [self checkForTurnEnd];    
}

 
-(NSMutableArray*)calculateAttackTiles
{
    NSMutableArray* ret = [[NSMutableArray alloc] init];
    for(NSValue* value in [_board neighbors:_coord]) {
        [ret addObject:value];
    }
    return ret;
}



////////////////////////////////////////////////////////////////////////////////



-(void) tick:(ccTime)dt
{
       
    [_battle setDynamicZ:self];
    
    
}

-(void) moveTo:(CGPoint)coord withPath:(NSMutableArray*)path
{
    [_board moveEntity:self from:_coord to:coord];
    movementReady = NO;
    
    [self runAction:[NavigateTo actionWithPath:path andNotification:@selector(moveToEndedNotification)]];

}

-(void) moveToEndedNotification
{
    [self runAnimation:kAnimationStance withFacing:currentFacing repeat:YES];    
    [self checkForTurnEnd];
}

-(void) checkForTurnEnd
{
    if(!movementReady && !attackReady) {
        [self finishedTurn];
    }
    
}

-(void)newTurn
{
    movementReady = YES;
    attackReady = YES;
    turnComplete = NO;
}

-(void) finishedTurn
{
    turnComplete = YES;
    [_battle entityTurnCompleteNotification:self];
}


////////////////////////////////////////////////////////////////////////////////

-(NSMutableDictionary*) calculatePaths
{
    
    typedef struct {
        int distance;
        CGPoint tile;
        NSMutableArray* path;        
    } WorklistElement;
    
    /*
            
    def calculate_paths(self, start_tile, start_distance, char):

        paths = {}

        worklist = [ (start_tile, start_distance, []) ]
        
        while len(worklist) > 0:
            tile, distance, path = worklist.pop()
            if distance < 0: continue
            if paths.has_key(tile): continue
            paths[tile] = path

            for child in [ tile.up(), tile.down(), tile.left(), tile.right() ]:
                if child is not None and char.can_travel(tile, child):
                    worklist.insert(0, (child, distance - 1, path + [child]))
                    
        return paths
        
     */
    
    NSMutableDictionary *paths = [[NSMutableDictionary alloc] init ];
    NSMutableArray *worklist = [[NSMutableArray alloc] init];
    
    WorklistElement current;
    current.path = [[NSMutableArray alloc] init];
    current.distance = _moveRange;
    current.tile = _coord;
    
    NSValue* value;
    value = [NSValue valueWithBytes:&current objCType:@encode(WorklistElement)];
    [worklist addObject:value];
    
    while ([worklist count] > 0) {
        // pop worklist into current
        value = [worklist lastObject];
        [worklist removeLastObject];
        [value getValue:&current];
        
        if (current.distance < 0) continue;
        if ([paths objectForKey:[NSValue valueWithCGPoint:current.tile]]) continue;
        [paths setObject:current.path forKey:[NSValue valueWithCGPoint:current.tile]];
        
        for(NSValue* childValue in [_board neighbors:current.tile]) {
            CGPoint childCoord = [childValue CGPointValue];
            if([_board walkable:childCoord byEntity:self]) {
                WorklistElement nextElement;
                nextElement.distance = current.distance - 1;
                nextElement.tile = childCoord;
                NSMutableArray* newPath = [[NSMutableArray alloc] initWithArray:current.path];
                [newPath addObject:childValue];
                nextElement.path = newPath;
                
                NSValue* nextValue = [NSValue valueWithBytes:&nextElement objCType:@encode(WorklistElement)];
                [worklist insertObject:nextValue atIndex:0];
                
            }
        }
        

    }
    
    NSArray* keys = [paths allKeys];
    for(NSValue* value in keys) {
        CGPoint tileCoord = unwrap(value);
        TileInfo* info = [_board tileInfoAt:tileCoord];
        Entity* entity = [info entity];
        if(entity != nil) {
            [paths removeObjectForKey:value];
        }
    }
    
    return paths;
}

@end

