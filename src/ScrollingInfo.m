//
//  ScrollingInfo.m
//  TownHero
//
//  Created by Danny Loffredo on 1/13/12.
//  Copyright 2012 Private. All rights reserved.
//

#import "ScrollingInfo.h"
#import "Entity.h"

@implementation ScrollingInfo


-(id)initWithImage:(NSString*)filename onEntity:(Entity*)node {
    if (self = [super init]) {
        
        [super attachTo:node atOffset:ccp(0,100)];
        
        info = [[CCSprite alloc] initWithFile:filename];
        CCMoveBy* scroll = [CCMoveBy actionWithDuration:0.5f position:ccp(0, 20)];
        CCCallFunc* call = [CCCallFunc actionWithTarget:self selector:@selector(animationEnded)];
        CCAction* action = [CCSequence actions:scroll, call, nil];
                       
        [info runAction:action];
        [self addChild:info];
        
        
    }
    
    return self;
}


-(void)animationEnded 
{
    [self.parent removeChild:self cleanup:YES];    
}


@end
