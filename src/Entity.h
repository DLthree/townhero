//
//  Player.h
//  TownHero
//
//  Created by Danny Loffredo on 10/28/11.
//  Copyright 2011 Private. All rights reserved.
//


#import "cocos2d.h"

@class Board;
@class BattleLayer;
@class EntityHud;

enum {
    kEntityNeutral,
    kEntityEnemy,
    kEntityPlayer,
};


@interface EntityInfo : NSObject {
    NSString* name;
    NSString* textureFile;
    NSString* framesFile;
    NSString* animationFile;
    NSNumber* health;
    NSNumber* meleeDamageMax;
    NSNumber* meleeDamageMin;
    NSNumber* moveRange;
    
}

@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* textureFile;
@property (nonatomic, retain) NSString* framesFile;
@property (nonatomic, retain) NSString* animationFile;
@property (nonatomic, retain) NSNumber* health;
@property (nonatomic, retain) NSNumber* meleeDamageMax;
@property (nonatomic, retain) NSNumber* meleeDamageMin;
@property (nonatomic, retain) NSNumber* moveRange;

@end



@interface Entity : CCSprite {

    int currentFacing;
    int currentAnimation;
    float moveSpeed;
    
    BattleLayer* _battle;
    NSString* _name;

    int healthCurrent;
    int healthMax;
    
    int _moveRange;

    CGPoint _coord;
    CGPoint anchor;
    
    int entityType;
    
    Board* _board;
    
    EntityHud* hud;
    
    int meleeDamageMin;
    int meleeDamageMax;
    float missChance;
    float critChance;
    
    BOOL movementReady;
    BOOL attackReady;
    BOOL turnComplete;
    BOOL alive;
    
    NSMutableArray *powers;
}

-(id) initWithName:(NSString*)name;

-(void) moveTo:(CGPoint)coord withPath:(NSMutableArray*)path;
-(void)attack:(CGPoint)coord;

-(void) meleeHit:(Entity*)entity;
-(int) calculateMeleeDamage;
-(void) receiveDamage:(int)damage from:(Entity*)entity delay:(float)delay;

-(NSMutableDictionary*) calculatePaths;
-(NSMutableArray*)calculateAttackTiles;

-(void) tick:(ccTime)dt;

-(void) newTurn;
-(void) finishedTurn;
-(void) moveToEndedNotification;
-(void) meleeAttackEndedNotification;
-(void) checkForTurnEnd;

-(void)runAnimation:(int)animation withFacing:(int)facing;
-(void)runAnimation:(int)animation withFacing:(int)facing withNotification:(SEL)s;
-(void)runAnimation:(int)animation withFacing:(int)facing repeat:(BOOL)repeat;
-(void)runAnimation:(int)animation withFacing:(int)facing withNotification:(SEL)s repeat:(BOOL)repeat;

+ (void)loadEntityResources:(EntityInfo*)info;


@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) EntityHud* hud;
@property (nonatomic, readwrite, assign) int healthCurrent;
@property (nonatomic, readwrite, assign) CGPoint coord;
@property (nonatomic, readwrite, assign) int healthMax;
@property (nonatomic, readwrite, assign) int entityType;
@property (nonatomic, retain) Board* board;

@property (nonatomic, readwrite, assign) int meleeDamageMin;
@property (nonatomic, readwrite, assign) int meleeDamageMax;
@property (nonatomic, readwrite, assign) float missChance;
@property (nonatomic, readwrite, assign) float critChance;

@property (nonatomic, readonly) BOOL movementReady;
@property (nonatomic, readonly) BOOL attackReady;
@property (nonatomic, readonly) BOOL alive;
@property (nonatomic, readonly) int currentAnimation;
@property (nonatomic, readonly) int currentFacing;
@property (nonatomic, readonly) float moveSpeed;
@property (nonatomic, readonly) BOOL turnComplete;

@property (nonatomic, retain) NSMutableArray* powers;


@end
