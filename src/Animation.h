//
//  Animation.h
//  TownHero
//
//  Created by Danny Loffredo on 10/28/11.
//  Copyright 2011 Private. All rights reserved.
//

#import "cocos2d.h"

enum {
//    kFacingWest = 0,
//    kFacingNorthWest = 1,
//    kFacingNorth = 2,
//    kFacingNorthEast = 3,
//    kFacingEast = 4,
//    kFacingSouthEast = 5,
//    kFacingSouth = 6,
//    kFacingSouthWest = 7,
    kFacingNorthWest = 0,
    kFacingNorthEast = 1,
    kFacingSouthEast = 2,
    kFacingSouthWest = 3,
};
#define NUM_FACINGS 4

enum {
    kAnimationStance,
    kAnimationRun,
    kAnimationMelee,
    kAnimationMent,
    kAnimationRanged,
    kAnimationBlock,
    kAnimationHit,
    kAnimationDie,
    kAnimationCritDie,
    kAnimationSpawn,
};

enum {
    kRepeatOnce,
    kRepeatLoop,
    kRepeatBounce,    
};

int getFacing(CGPoint p);
int repeatNumber(NSString* name);
int animationNumber(NSString* name);
CCAnimation* actionFromDictionary(NSDictionary* dict, int facing, NSString* name, int frames_x, int frames_y);
void animationsFromPlist(NSString* name, NSString* filename);
NSString* animationName(NSString* name, int facing, int animation);
