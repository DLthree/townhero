//
//  UILayer.m
//  TownHero
//
//  Created by Danny Loffredo on 11/29/11.
//  Copyright 2011 Private. All rights reserved.
//

#import "UILayer.h"
#import "ActionMenu.h"
#import "Battle.h"
#import "Power.h"
#import "CCMenuAdvanced.h"

@implementation UILayer

@synthesize battle, menu;

#define MENU_POSITION winPoint(0.8f, 0.9f)
#define MENU_START winPoint(1.f, 0.9f)

#define MESSAGE_FONT_NAME @"Diavlo_BLACK_II_37.otf"
#define MESSAGE_FONT_SIZE 48

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
    
	// don't forget to call "super dealloc"
	[super dealloc];
}

////////////////////////////////////////////////////////////////////////////////

// on "init" you need to initialize your instance
-(id) init
{
    
    if((self = [super init])) {
        menu = nil;
        messageLabel = nil;
        self.isTouchEnabled = YES;
        _touching = NO;
        _tap = NO;
        
    }
    return self;
}

-(void)showMenu:(Entity*)entity
{
    if(menu != nil) {
        [self removeChild:menu cleanup:YES];
    }
    menu = [[ActionMenu alloc] initWithDelegate:self entity:entity];
    CGPoint menuPosition = [[battle board] convertToWorldSpace:[entity position]];

    menu.position = menuPosition;
    CGRect bb = [menu boundingBox];
    CGPoint fixup = clampToScreen(bb);
    menu.position = ccpAdd(menuPosition, fixup);
    
    [self addChild:menu];

//    [menu setScale:0.01f];
//    CCScaleTo* scaleIn = [CCScaleTo actionWithDuration:0.2f scale:1.0f];
//    [menu runAction:scaleIn];
    CCFadeIn* fadeIn = [CCFadeIn actionWithDuration:0.3f];
    [menu runAction:fadeIn];

}
			
-(void)hideMenu
{
    menu.menu.isTouchEnabled = NO;
    
    CCCallBlock* call = [CCCallBlock actionWithBlock:^{
        [self removeChild:menu cleanup:YES];
        menu = nil;
    }];
    
    
    CCFadeOut *fadeOut = [CCFadeOut actionWithDuration:0.3f];
    CCAction *seq = [CCSequence actions:fadeOut, call, nil];
    [menu runAction:seq];
    
}

-(void)showMessage:(NSString*)msg
{
    if(messageLabel) {
        [self removeChild:messageLabel cleanup:YES];
    }
    messageLabel = [CCLabelTTF labelWithString:msg fontName:MESSAGE_FONT_NAME fontSize:MESSAGE_FONT_SIZE];
    messageLabel.position = winPoint(0.5f, 0.5f);
    [self addChild:messageLabel];
    CCDelayTime *delay = [CCDelayTime actionWithDuration:2.0f];
    CCFadeOut *fadeOut = [CCFadeOut actionWithDuration:1.0f];
    CCAction *seq = [CCSequence actions:delay, fadeOut, nil];
    [messageLabel runAction:seq];

}

-(Entity*)entityAtCoord:(CGPoint)coord
{
    TileInfo* info = [battle.board tileInfoAt:coord];
    Entity* e = [info entity];
    
    return e;
}

-(void)powerSelected:(CCMenuItem*)sender
{
    [self hideMenu];
    Power* power = [sender userData];
    NSLog(@"%@", power.icon);    
}

-(void)tapAt:(CGPoint)coord
{
    TileInfo* info = [battle.board tileInfoAt:coord];
    Entity* e = [info entity];
    
    if(e == nil) return;
    
    if([e isKindOfClass:[Player class]]) {
        [self showMenu:e];
    }
    
    
}


-(void) dragFrom:(CGPoint)oldCoord to:(CGPoint)newCoord
{
    if(!touchEntity || ![touchEntity movementReady]) return;
    
    [battle.board colorAllTiles:NO_COLOR];
    
    CGPoint position = [[battle board] positionFromTileCoord:[touchEntity coord]];
    [touchEntity setPosition:position];
    
    NSMutableArray* path = [paths objectForKey:wrap(newCoord)];
    if(path) {
        [touchEntity moveTo:newCoord withPath:path];
    }        
    
}

-(void) previewDragFrom:(CGPoint)oldCoord to:(CGPoint)newCoord initialize:(BOOL)initialize
{
    if(!touchEntity || ![touchEntity movementReady]) return;

    if(initialize) {
        paths = [touchEntity calculatePaths];
        [battle.board colorAllTiles:DARK_HIGHLIGHT];
        [battle.board colorTiles:[paths allKeys] color:BLUE_COLOR];
    }
    
    NSMutableArray* path = [paths objectForKey:wrap(newCoord)];
    if(path) {
        CGPoint position = [[battle board] positionFromTileCoord:newCoord];
        [touchEntity setPosition:position];
    } else {
        CGPoint position = [[battle board] positionFromTileCoord:[touchEntity coord]];
        [touchEntity setPosition:position];
    }
    
    
}

-(void) previewDragFrom:(CGPoint)oldCoord to:(CGPoint)newCoord
{
    [self previewDragFrom:oldCoord to:newCoord initialize:NO];
}


-(void) registerWithTouchDispatcher
{
    // lowest priority number goes first...I want this guy to be the last in the queue
	[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self 
                                                     priority:1000 swallowsTouches:YES];
}

-(CGPoint)touchToCoord:(UITouch*)touch
{
    CGPoint touchLocation = [touch locationInView: [touch view]];		
    touchLocation = [[CCDirector sharedDirector] convertToGL: touchLocation];
    touchLocation = [battle convertToNodeSpace:touchLocation];
    CGPoint coord = [battle.board tileCoordFromPosition:touchLocation];
    return coord;
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    
    _tap = YES;
    _touching = YES;
   
    touchCoord = [self touchToCoord:touch];
    touchEntity = [self entityAtCoord:touchCoord];
    prevCoord = touchCoord;
    [self hideMenu];

    return YES;
}

-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
    //    CGPoint touchLocation = [touch locationInView: [touch view]];
    //    CGPoint prevLocation = [touch previousLocationInView: [touch view]];
    //    touchLocation = [[CCDirector sharedDirector] convertToGL: touchLocation];
    //    prevLocation = [[CCDirector sharedDirector] convertToGL: prevLocation];
    //    CGPoint diff = ccpSub(prevLocation,touchLocation);
    
    CGPoint coord = [self touchToCoord:touch];

    if(_tap && (coord.x != touchCoord.x || coord.y != touchCoord.y)) {
        _tap = NO;
        [self previewDragFrom:touchCoord to:coord initialize:YES];
    } else if(!_tap && (prevCoord.x != coord.x || prevCoord.y != coord.y)) {
        [self previewDragFrom:touchCoord to:coord];
    }

    prevCoord = coord;
    
}


-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    _touching = NO;

    CGPoint coord = [self touchToCoord:touch];
    
    if(_tap) {
        [self tapAt:coord];
    } else {
        [self dragFrom:touchCoord to:coord];
    }
    
}



@end

