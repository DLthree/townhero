//
//  EntityHud.h
//  TownHero
//
//  Created by Danny Loffredo on 1/11/12.
//  Copyright 2012 Private. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "LifeBar.h"
#import "AttachableNode.h"

@class Entity;

@interface EntityHud : AttachableNode {
    
    LifeBar* healthBar;
    Entity* entity;
    
}

-(id)initWithEntity:(Entity*)node;

@end
