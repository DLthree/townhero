//
//  Player.m
//  TownHero
//
//  Created by Danny Loffredo on 11/29/11.
//  Copyright (c) 2011 Private. All rights reserved.
//

#import "Player.h"
#import "Battle.h"
#import "Animation.h"
#import "Enemy.h"

@implementation Player

// on "init" you need to initialize your instance
-(id) initWithName:(NSString*)name
{
   
    if((self = [super initWithName:name])) {
        entityType = kEntityPlayer;
        
    }
    return self;
}


-(void) tick:(ccTime)dt
{
   
    
    [super tick:dt];
    
}


@end
