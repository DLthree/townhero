//
//  Enemy.m
//  TownHero
//
//  Created by Danny Loffredo on 11/29/11.
//  Copyright (c) 2011 Private. All rights reserved.
//

#import "Enemy.h"
#import "Animation.h"
#import "Battle.h"

@implementation Enemy

// on "init" you need to initialize your instance
-(id) initWithName:(NSString*)name
{
    
    if((self = [super initWithName:name])) {
        entityType = kEntityEnemy;
    }
    return self;
}

-(void)onEnter
{
    [super onEnter];
}

-(void) tick:(ccTime)dt
{
 

    
    [super tick:dt];
}


@end
