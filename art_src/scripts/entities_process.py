#!/usr/bin/python

import os
import sys
import string
pydir = os.path.dirname(os.path.abspath(__file__))
dstdir = os.path.abspath("%s/../../game_data/entities" % pydir)
convert = os.path.abspath("%s/../../deps/ImageMagick-6.7.3/bin/convert" % pydir)
os.chdir("%s/../entities/out" % pydir)

def cmd(s):
    print s
    ret = os.system(s)
    if ret != 0: raise Exception("Failed: %d" % ret)

def generate(lst, outd, name):
    out = "%s/%s" % (outd, name)
    tmp = "%s.png" % out
    infiles = string.join(lst, " ")
    plist = "%s.plist" % out
    final = "%s.pvr.ccz" % out
    
    cmd("%s -background transparent %s -flatten %s"
        % (convert, infiles, tmp))
    cmd("mkdir -p %s" % out)
    #cmd("%s %s -crop 128x128 %s/%s%%d.png" % (convert, tmp, out, name))
    cmd("%s %s -crop 256x256 %s/%s%%d.png" % (convert, tmp, out, name))
    cmd("rm -f %s" % tmp)

    #cmd("TexturePacker                   --opt RGBA4444 --scale 0.75 --premultiply-alpha --format cocos2d %s --data %s --sheet %s" % (out, plist, final))
    #cmd("TexturePacker                   --opt RGBA8888 --scale 0.75 --premultiply-alpha --format cocos2d %s --data %s --sheet %s" % (out, plist, final))
    #cmd("TexturePacker --dither-fs-alpha --opt RGBA4444 --scale 0.75 --premultiply-alpha --format cocos2d %s --data %s --sheet %s" % (out, plist, final))
    cmd("TexturePacker --dither-fs-alpha --opt RGBA8888 --scale 0.75 --premultiply-alpha --format cocos2d %s --data %s --sheet %s" % (out, plist, final))

    cmd("rm -rf %s" % out)

    cmd("plutil -convert binary1 %s" % plist)
    




# male_heads = ["hero/male_head1.png", "hero/male_head2.png", "hero/male_head3.png"]
# male_mains = [ "hero/dagger.png", "hero/greatstaff.png", "hero/greatsword.png", "hero/longsword.png", "hero/rod.png", "hero/shortsword.png", "hero/staff.png", "hero/wand.png", "hero/staff.png"]
# male_offs = [ "hero/buckler.png", "hero/greatbow.png",  "hero/longbow.png", "hero/shield.png", "hero/shortbow.png", "hero/slingshot.png", None ]
# male_bodys = [ "hero/clothes.png", "hero/leather_armor.png", "hero/steel_armor.png" ]

# female_heads = ["heroine/head_long.png"]
# female_mains = [ "heroine/dagger.png", "heroine/greatstaff.png", "heroine/greatsword.png", "heroine/longsword.png", "heroine/rod.png", "heroine/shortsword.png", "heroine/staff.png", "heroine/wand.png", "heroine/staff.png" ]
# female_offs = [ "heroine/buckler.png", "heroine/greatbow.png",  "heroine/longbow.png", "heroine/shield.png", "heroine/shortbow.png", "heroine/slingshot.png" ]
# female_bodys = [ "heroine/clothes.png", "heroine/leather_armor.png", "heroine/steel_armor.png" ]


generate(["hero/steel_armor.png",
           "hero/male_head1.png",
           "hero/greatsword.png",
           "hero/shield.png"],
           dstdir,
           "knight")

# generate(["hero/steel_armor.png",
#            "hero/male_head1.png",
#            "hero/greatsword.png"],
#            dstdir,
#            "cleric")

generate(["heroine/leather_armor.png",
           "heroine/head_long.png",
           "heroine/shortsword.png",
           "heroine/greatbow.png"],
           dstdir,
           "rogue")

generate(["heroine/clothes.png",
           "heroine/head_long.png",
           "heroine/greatstaff.png"],
           dstdir,
           "mage")


generate(["antlion/antlion.png"],
         dstdir,
         "antlion")
generate(["goblin/goblin.png"],
         dstdir,
         "goblin")
generate(["goblin_elite/goblin_elite.png"],
         dstdir,
         "goblin_elite")
generate(["minotaur/minotaur.png"],
         dstdir,
         "minotaur")
generate(["zombie/zombie.png"],
         dstdir,
         "zombie")
generate(["skeleton_weak/skeleton_weak.png"],
         dstdir,
         "skeleton_weak")
generate(["skeleton_warrior/skeleton.png"],
         dstdir,
         "skeleton_warrior")
generate(["skeletal_mage/skeletal_mage.png"],
         dstdir,
         "skeleton_mage")
generate(["skeletal_archer/skeletal_archer.png"],
         dstdir,
         "skeleton_archer")
