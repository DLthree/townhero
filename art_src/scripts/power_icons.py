#!/usr/bin/python

import os
import sys
import string
pydir = os.path.dirname(os.path.abspath(__file__))
dstdir = os.path.abspath("%s/../../game_data/power_icons" % pydir)
convert = os.path.abspath("%s/../../deps/ImageMagick-6.7.3/bin/convert" % pydir)
indir = os.path.abspath("%s/../icons" % pydir)
    
def cmd(s):
    print s
    ret = os.system(s)
    if ret != 0: raise Exception("Failed: %d" % ret)

def generate(lst, outd, framename):
#    tmp = "%s.png" % out
#    plist = "%s.plist" % out
#    final = "%s.pvr.ccz" % out

    for f in lst:
        out = "%s/%s" % (outd, f)
        inf = "%s/%s" % (indir, f)
        frame = "%s/%s" % (indir, framename)
        cmd("%s -flatten -scale 42x42 %s %s %s" % (convert, inf, frame, out))

#    cmd("TexturePacker --opt RGBA4444 --premultiply-alpha --format cocos2d %s --data %s --sheet %s" % (out, plist, final))
#    cmd("rm -rf %s" % out)
#    cmd("plutil -convert binary1 %s" % plist)


infiles = """
enchant-red-3.png      ice-sky-3.png          runes-magenta-1.png
explosion-red-2.png    lighting-eerie-3.png   shielding-spirit-3.png
fire-arrows-3.png      link-royal-2.png       slice-sky-3.png
fireball-red-1.png     needles-acid-1.png     wild-acid-2.png
heal-jade-2.png        protect-orange-2.png   wind-grasp-sky-1.png
"""

generate(infiles.split(), dstdir, "frame-3-grey.png")

