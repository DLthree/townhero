#!/usr/bin/python

import sys
import sqlite3

stats = {}
# health, meleeDamageMin, meleeDamageMax, moveRange
stats["knight"] = [100, 10, 20, 5]
stats["mage"] = [100, 10, 20, 5]
stats["rogue"] = [100, 10, 20, 5]
stats["antlion"] = [100, 10, 20, 5]
stats["zombie"] = [100, 10, 20, 5]
stats["goblin"] = [100, 10, 20, 5]
stats["goblin_elite"] = [100, 10, 20, 5]
stats["skeleton_warrior"] = [100, 10, 20, 5]
stats["skeleton_weak"] = [100, 10, 20, 5]
stats["skeleton_mage"] = [100, 10, 20, 5]
stats["skeleton_archer"] = [100, 10, 20, 5]
stats["minotaur"] = [100, 10, 20, 5]

def build_entity(name, textureFile=None, framesFile=None, animationFile=None):
    entry = []
    if not textureFile:
        textureFile = "%s.pvr.ccz" % name
    if not framesFile:
        framesFile = "%s.plist" % name
    if not animationFile:
        animationFile = "%s_animation.plist" % name
    entry.append(name)
    entry.append(textureFile)
    entry.append(framesFile)
    entry.append(animationFile)
    for stat in stats[name]:
        entry.append(stat)
    return entry

entities = [build_entity("knight"),
            build_entity("mage"),
            build_entity("rogue"),
            build_entity("antlion"),
            build_entity("zombie"),
            build_entity("goblin"),
            build_entity("goblin_elite"),
            build_entity("skeleton_warrior"),
            build_entity("skeleton_weak"),
            build_entity("skeleton_mage"),
            build_entity("skeleton_archer"),
            build_entity("minotaur"),
            ]

conn = sqlite3.connect(sys.argv[1])
c = conn.cursor()

c.execute("drop table entities")
c.execute("create table entities (name, textureFile, framesFile, animationFile, health, meleeDamageMin, meleeDamageMax, moveRange)")

for entity in entities:
    c.execute("insert into entities values (?,?,?,?,?,?,?,?)", entity)

conn.commit()
c.close()

