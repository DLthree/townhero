#!/usr/bin/python

import os
import string
pydir = os.path.dirname(os.path.abspath(__file__))
os.chdir("%s/../entities" % pydir)

blender = '/Applications/blender.app/Contents/MacOS/blender'
montage = os.path.abspath("%s/../../deps/ImageMagick-6.7.3/bin/montage" % pydir)

failures = []

def cmd(s):
    print s
    ret = os.system(s)
    if ret == 0: return
    if ret < 255:
        f = "Failed: %s %d" % (s, ret)
        failures.append(f)
        print f
    else:
        raise Exception("Failed: %d" % ret)


def render(dir, files, new_name):
    curdir = os.getcwd()
    os.chdir(dir)
    outdir = "%s/out/%s" % (curdir, new_name)
    for name in files:
        cmd("%s -b %s.blend -o %s/%s_ -P ../render8dirs.py" % (blender, name, outdir, name))
        cmd("%s -background transparent -geometry +0+0 -tile x4 %s/%s_*.png %s/%s.png" % (montage, outdir, name, outdir, name))
    os.chdir(curdir)

render("skeleton", ["skeleton"], "skeleton_warrior")
render("skeleton", ["skeleton_weak"], "skeleton_weak")
render("skeleton", ["skeletal_archer"], "skeletal_archer")
render("skeleton", ["skeletal_mage"], "skeletal_mage")
render("antlion", ["antlion"], "antlion")
render("goblin", ["goblin"], "goblin")
render("goblin", ["goblin_elite"], "goblin_elite")
render("zombie", ["zombie"], "zombie")
render("minotaur", ["minotaur"], "minotaur")

render("hero", string.split("""
buckler       greatsword    male_head2    shortsword   
clothes       leather_armor male_head3    slingshot
dagger        longbow       rod           staff
greatbow      longsword     shield        steel_armor
greatstaff    male_head1    shortbow      wand
"""), "hero")


render("heroine", string.split("""
buckler       greatstaff    longbow       shortbow      steel_armor
clothes       greatsword    longsword     shortsword    wand
dagger        head_long     rod           slingshot
greatbow      leather_armor shield        staff
"""), "heroine")


print "Failures: %d" % len(failures)
for failure in failures:
    print failure
