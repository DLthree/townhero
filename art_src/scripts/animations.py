#!/usr/bin/python

import plistlib


import os
import sys
import string
import re

def cmd(s):
    print s
    ret = os.system(s)
    if ret != 0: raise Exception("Failed: %d" % ret)


def generate(inf, outf):
    d = {}

    name = "render"
    entry = {}

    f = file(inf, "r")
    for line in f.readlines():
        line = line.strip()

        mo = re.match("(\w+)=(\w+)", line)
        if mo:
            key = mo.group(1)
            try:
                val = int(mo.group(2))
            except ValueError:
                val = mo.group(2)
            print name, key, val
            entry[key] = val
            continue
            
        mo = re.match("\[(\w+)\]", line)
        if mo:
            d[name] = entry
            
            name = mo.group(1)
            entry = {}
            continue

        if line:
            print "skipped %s" % line

    d[name] = entry

    plistlib.writePlist(d, outf)
    cmd("plutil -convert binary1 %s" % outf)

generate("../entities/animations/hero.txt", "../../game_data/entities/knight_animation.plist")
#generate("../entities/animations/hero.txt", "../../game_data/entities/cleric_animation.plist")
generate("../entities/animations/hero.txt", "../../game_data/entities/rogue_animation.plist")
generate("../entities/animations/hero.txt", "../../game_data/entities/mage_animation.plist")

generate("../entities/animations/antlion.txt", "../../game_data/entities/antlion_animation.plist")
generate("../entities/animations/goblin_runner.txt", "../../game_data/entities/goblin_elite_animation.plist")
generate("../entities/animations/goblin.txt", "../../game_data/entities/goblin_animation.plist")
generate("../entities/animations/minotaur.txt", "../../game_data/entities/minotaur_animation.plist")
generate("../entities/animations/zombie.txt", "../../game_data/entities/zombie_animation.plist")
generate("../entities/animations/skeleton.txt", "../../game_data/entities/skeleton_archer_animation.plist")
generate("../entities/animations/skeleton_mage.txt", "../../game_data/entities/skeleton_weak_animation.plist")
generate("../entities/animations/skeleton_mage.txt", "../../game_data/entities/skeleton_mage_animation.plist")
generate("../entities/animations/skeleton_mage.txt", "../../game_data/entities/skeleton_warrior_animation.plist")
