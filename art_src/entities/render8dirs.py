import bpy
from math import radians

angle = 90
axis = 2 # z-axis
platform = bpy.data.objects["RenderPlatform"]
original_path = bpy.data.scenes[0].render.filepath

bpy.data.scenes[0].render.resolution_x *= 2
bpy.data.scenes[0].render.resolution_y *= 2
# bpy.data.scenes[0].render.use_motion_blur = True
# bpy.data.scenes[0].render.motion_blur_samples = 16
# bpy.data.scenes[0].render.motion_blur_shutter = 0.5

bpy.ops.render.view_show()


# temp_rot = platform.rotation_euler
# temp_rot[axis] = temp_rot[axis] - radians(45)
# platform.rotation_euler = temp_rot;

for i in range(0,4):
	
	# rotate the render platform and all children
	temp_rot = platform.rotation_euler
	temp_rot[axis] = temp_rot[axis] - radians(angle)
	platform.rotation_euler = temp_rot;
	
	# set the filename direction prefix
	bpy.data.scenes[0].render.filepath = original_path + str(i)
	
	# render animation for this direction
	bpy.ops.render.render(animation=True)

bpy.data.scenes[0].render.filepath = original_path
